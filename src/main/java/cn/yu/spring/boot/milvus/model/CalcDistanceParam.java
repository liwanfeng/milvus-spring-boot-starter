package cn.yu.spring.boot.milvus.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zy
 * @date 2022/7/26 11:14
 * @desc 描述
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CalcDistanceParam {
    private Float vectorsLeft;
    private Float vectorsRight;
}
