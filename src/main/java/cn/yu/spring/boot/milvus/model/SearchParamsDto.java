package cn.yu.spring.boot.milvus.model;

import lombok.*;

/**
 * @author zhang yu
 * @version 1.0
 * @date 2022/12/26 13:57
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SearchParamsDto {
    private Integer nprobe;
    private Integer offset;
    private Integer limit;

}
