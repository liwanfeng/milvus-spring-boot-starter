package cn.yu.spring.boot.milvus.model;

import io.milvus.grpc.DataType;
import lombok.*;

import java.util.List;

/**
 * @author zy
 * @date 2022/4/2 1:48 下午
 * @desc 描述
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FieldDto {

    private String name;
    private DataType dataType;
    private List<?> values;

}
